public enum Tile {
	BLANK("_"),
	WALL("W"),
	HIDDEN_WALL("_"),
	CASTLE("C");
	
	//FIELDS
	private final String name;
	
	//CONSTRUCTOR
	private Tile(String name) {
		this.name = name;
	}
	
	//GET METHOD
	public String getName() {
		return this.name;
	}
}