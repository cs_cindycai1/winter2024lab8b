import java.util.Scanner;

public class BoardGameApp {
	public static void main(String[] agrs) {
		Scanner reader = new Scanner(System.in);
		System.out.println("Welcome to the Castle Game!"); //initializes game settings
		int numCastles = 7;
		int turns = 0;
		Board castleGame = new Board();
		
		while (numCastles > 0 && turns < 8) { 
			System.out.println(castleGame);
			
			System.out.println("Input the row you wish to put your castle:"); 
			int row = reader.nextInt();
			System.out.println("Input the column you wish to put your castle:");
			int col = reader.nextInt();
			
			int validPosition = castleGame.placeToken(row, col);
			
			while (validPosition < 0) { //ask for a valid input while validPosition is outside range or in an occupied position
				System.out.println("Input a valid row you wish to put your castle: ");
				row = reader.nextInt();
				System.out.println("Input a valid column you wish to put your castle: ");
				col = reader.nextInt();
				validPosition = castleGame.placeToken(row, col);
			}
			
			if (validPosition == 1) { //display if castle was placed or not
				System.out.println("There was a hidden wall here!");
			} else {
				System.out.println("Yur castle tile was placed successfully!");
				numCastles--;
			}
			turns++;
		}
		System.out.println(castleGame);
		
		if (numCastles == 0) { //display result of game
			System.out.println("You won!");
		} else {
			System.out.println("You lost");
		}
	}
}