import java.util.Random;

public class Board {
	//FIELDS
	private Tile[][] grid;
	
	//CONSTRUCTOR
	public Board() {
		Random rand = new Random();
		int size = 5;
		
		grid = new Tile[size][size];
		
		for (int row = 0; row < grid.length; row++) { //for each row
			for (int col = 0; col < grid[row].length; col++) { //for each column
				grid[row][col] = Tile.BLANK;
			}
		}
		
		int hiddenWallNumber = rand.nextInt(5, 10);
		while (hiddenWallNumber > 0) { //add HIDDEN_WALL in random places
			int randRow = rand.nextInt(this.grid.length);
			int randCol = rand.nextInt(this.grid[randRow].length);
			this.grid[randRow][randCol] = Tile.HIDDEN_WALL;
			hiddenWallNumber--;
		}
	}
	
	//TOSTRING
	public String toString() {
		String result = "";
		
		for (int i = 0; i < this.grid.length; i++) {
			for (int j = 0; j < this.grid[i].length; j++) {
				result += this.grid[i][j].getName() + " ";
			}
			result += "\n";
		}
		
		return result;
	}
	
	//CUSTOM METHOD
	
	//place token at position if position is available
	public int placeToken(int row, int col) {
		if (row < 0 || row >= this.grid.length || col < 0 || col >= this.grid.length) {
			return -2;
		} 
		
		Tile position = this.grid[row][col];
		
		if (position == Tile.CASTLE || position == Tile.WALL) {
			return -1;
		} else if (position == Tile.HIDDEN_WALL) {
			this.grid[row][col] = Tile.WALL;
			return 1;
		} else {
			this.grid[row][col] = Tile.CASTLE;
			return 0;
		}
	}
}